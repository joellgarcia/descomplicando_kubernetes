# descomplicando_kubernetes

Repositorio dos arquivos gerados no curso [Descomplicando o Kubernetes - Expert Mode](https://www.linuxtips.io/course/descomplicando-o-kubernetes-expert-mode) da [LINUXtips](https://www.linuxtips.io/).

### [Livro - Descomplicando o Kubernetes](https://livro.descomplicandokubernetes.com.br/)

### [Repositório - DescomplicandoKubernetes](https://github.com/badtuxx/CertifiedContainersExpert/tree/main/DescomplicandoKubernetes)
